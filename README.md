# Year Update

```bash
#!/bin/bash

dir=$1
old_year=$2
new_year=$3

if [[ $# -ne 3 || ! -d $dir ]]; then
  echo "Usage: <dir> <old_year> <new_year>"
  exit 1
fi

files=$(find "$dir" -type f -name "*.rs")

for file in $files; do
  if [[ -f $file ]]; then
    sed -i "1,5s/$old_year/$new_year/g" "$file"
  fi
done

echo "Successful replace"
```

```bash
chmod +x year_update.sh
```

```bash
sudo mv year_update.sh /usr/local/bin/year_update
```

## Usage

```bash
Usage: <dir> <old_year> <new_year>
```